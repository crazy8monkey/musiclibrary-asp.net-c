﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MusicDirectory.Models;


namespace MusicDirectory.DAL
{
	public class MusicDirectoryDAL : DbContext
	{
		public DbSet<ArtistModel> MusicArtist { get; set; }
		public DbSet<AlbumModel> MusicArtistAlbum { get; set;}
        public DbSet<GenreModel> MusicGenres { get; set; }
        public DbSet<MusicTrackModel> MusicTrack { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ArtistModel>().ToTable("MusicArtistsTbl");
			modelBuilder.Entity<AlbumModel>().ToTable("AlbumsTbl");
            modelBuilder.Entity<GenreModel>().ToTable("GenreTbl");
            modelBuilder.Entity<MusicTrackModel>().ToTable("MusicFiles");
			base.OnModelCreating(modelBuilder);
		}


	}
}
  