﻿using System;
using System.Diagnostics.Contracts;
using System.IO;

namespace MusicDirectory.Libraries
{
	public class FolderDirectory
	{
		private string DirectoryPath = @"MusicFiles/";

		public void CreateArtistDirectory(string FolderName)
		{
			if (!Directory.Exists(String.Format("{0}{1}", DirectoryPath, FolderName)))
			{
				Directory.CreateDirectory(String.Format("{0}{1}", DirectoryPath, FolderName));
			}
		}

		public void DeleteArtistDirectory(string FolderName)
		{
			var ArtistDirectory = new DirectoryInfo(String.Format("{0}{1}", DirectoryPath, FolderName));
			ArtistDirectory.Delete(true);
		}
	}
}
