﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;


namespace MusicDirectory.Libraries
{
    public class FileUpload
    {
        private string DirectoryPath = @"MusicFiles/";

        public void UploadAlbumArtWork(string ArtistFolderName, string AlbumFolderName, HttpPostedFileBase AlbumArtImage) {
            var fileName = Path.GetFileName(AlbumArtImage.FileName);
            AlbumArtImage.SaveAs(String.Format("{0}{1}/{2}", DirectoryPath, ArtistFolderName, AlbumFolderName));
        }
    }
}
