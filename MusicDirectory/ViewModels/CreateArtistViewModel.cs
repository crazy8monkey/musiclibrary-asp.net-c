﻿using System;
using MusicDirectory.Models;
using System.Collections.Generic;

namespace MusicDirectory.ViewModels
{
    public class CreateArtistViewModel
    {
        public string ArtistName { get; set; }
        public int Genre { get; set; }

        public List<GenreModel> GenreList { get; set; }
    }
}
