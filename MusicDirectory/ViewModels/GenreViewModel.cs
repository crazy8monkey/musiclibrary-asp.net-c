﻿using System;
using System.Collections.Generic;
using MusicDirectory.Models;
namespace MusicDirectory.ViewModels
{
    public class GenreViewModel
    {
		public int genreID { get; set; }
		public string GenreText { get; set; }

        public virtual List<ArtistModel> Artists { get; set; }

    }
}
