﻿using System;
using System.Collections.Generic;
using System.Web;
using MusicDirectory.Models;

namespace MusicDirectory.ViewModels
{
    public class AlbumViewModel
    {
        public int AlbumID { get; set; }
        public string AlbumName { get; set; }
        public int ArtistID { get; set; }
        public string AlbumArtwork { get; set; }
        public string AlbumFolderName { get; set; }

        public ArtistModel Artist { get; set; }

        public List<ArtistModel> Artists { get; set; }
        public List<MusicTrackViewModel> Tracks { get; set; }
    }
}
