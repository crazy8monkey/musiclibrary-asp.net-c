﻿using System;
using MusicDirectory.Models;
using System.Collections.Generic;

namespace MusicDirectory.ViewModels
{
    public class GenreListViewModel
    {
       public List<GenreViewModel> GenreList { get; set; }
    }
}
