﻿using System;
namespace MusicDirectory.ViewModels
{
    public class MusicTrackListViewModel
    {
		public int musicFileID { get; set; }
		public string SongName { get; set; }
		public int AlbumID { get; set; }
    }
}
