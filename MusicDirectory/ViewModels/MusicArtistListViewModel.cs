﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicDirectory.ViewModels
{
	public class MusicArtistListViewModel
	{
		public List<MusicArtistViewModel> MusicArtists { get; set;}
	}
}
