﻿using System;
namespace MusicDirectory.ViewModels
{
    public class MusicTrackViewModel
    {
        public int musicFileID { get; set; }
        public string SongName { get; set; }
        public int? trackOrder { get; set; }    
    }
}
