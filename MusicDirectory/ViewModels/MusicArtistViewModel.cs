﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MusicDirectory.Models;

namespace MusicDirectory.ViewModels
{
	public class MusicArtistViewModel
	{
		public int ArtistID { get; set; }
		public string ArtistName { get; set; }
		public string FolderName { get; set; }

		public virtual List<AlbumModel> Albums { get; set;}


	}
}
