﻿using System;
using System.Collections.Generic;
using System.Linq;
using MusicDirectory.DAL;

namespace MusicDirectory.Models
{
    public class GenreBusinessLayer
    {
       private MusicDirectoryDAL musicDal = new MusicDirectoryDAL();

        public GenreModel SaveNewGenre(GenreModel genre) {
            musicDal.MusicGenres.Add(genre);
            musicDal.SaveChanges();
            return genre;
        }


		public GenreModel EditGenre(GenreModel genre)
		{
			var SingleGenre = musicDal.MusicGenres.FirstOrDefault(a => a.genreID == genre.genreID);
			SingleGenre.GenreText = genre.GenreText;
			musicDal.SaveChanges();
			return genre;
		}

        public List<GenreModel> GetGenreLists() {
            return musicDal.MusicGenres.ToList();
        }

        public GenreModel SingleGenre(int ID) {
            return musicDal.MusicGenres.Find(ID);
        }
    }
}
