﻿using System;
using MusicDirectory.DAL;
using MusicDirectory.Models;
using MusicDirectory.Libraries;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Net;
using System.Diagnostics.Contracts;

namespace MusicDirectory.Models
{
	public class AlbumBusinessLayer
	{
		private MusicDirectoryDAL musicDal = new MusicDirectoryDAL();
		private FolderDirectory AlbumFolder = new FolderDirectory();

		private string toFriendly(string subject)
		{
			subject = subject.Trim().ToLower();
			subject = Regex.Replace(subject, @"\s+", "-");
			subject = Regex.Replace(subject, @"[^A-Za-z0-9_-]", "");
			return subject;
		}


		public AlbumFileUploadModel CreateNewAlbum(AlbumFileUploadModel album)
		{
			//adaption of this
			//https://stackoverflow.com/questions/20833479/in-mvc4-file-upload-its-saving-system-web-httppostedfilewrapper-in-db-instead
			var SingleArtist = musicDal.MusicArtist.FirstOrDefault(a => a.ArtistID == album.ArtistID);
			AlbumFolder.CreateArtistDirectory(String.Format("{0}/{1}", SingleArtist.FolderName, toFriendly(album.AlbumName)));

            if(album.AlbumArtwork != null) {
                var fileName = Path.GetFileName(album.AlbumArtwork.FileName);
                var path = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/MusicFiles/{0}/{1}", SingleArtist.FolderName, toFriendly(album.AlbumName))), fileName);
                album.AlbumArtwork.SaveAs(path);

				musicDal.MusicArtistAlbum.Add(new AlbumModel
				{
					AlbumArtwork = album.AlbumArtwork.FileName,
					AlbumName = album.AlbumName,
					AlbumFolderName = toFriendly(album.AlbumName),
					ArtistID = album.ArtistID
				});
            } else {
				musicDal.MusicArtistAlbum.Add(new AlbumModel
				{
					AlbumName = album.AlbumName,
					AlbumFolderName = toFriendly(album.AlbumName),
					ArtistID = album.ArtistID
				});
            }
           	
			musicDal.SaveChanges();
			return album;
		}

        public AlbumModel GetSingleAlbum(int ID) {
            return musicDal.MusicArtistAlbum.Find(ID);
        }


		public AlbumModel GetAlbumsByArtist(int ArtistID)
		{
			var MusicAlbums = new List<AlbumModel>();
			return MusicAlbums.Find(x => x.ArtistID == ArtistID);
		}
	}
}
