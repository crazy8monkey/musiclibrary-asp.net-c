﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MusicDirectory.Models
{
    public class MusicTrackModel
    {
        [Key]
        public int musicFileID { get; set; }
        public string SongName { get; set; }
        public int AlbumID { get; set; }
        public int? trackOrder { get; set; }
    }
}
