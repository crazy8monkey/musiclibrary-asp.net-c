﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MusicDirectory.Models;

namespace MusicDirectory.Models
{
    public class ArtistModel
    {
        [Key]
        public int ArtistID { get; set; }
        [Display(Name = "Artist Name")]
        [Required(ErrorMessage="Artist Name is Required")]
        public string ArtistName { get; set; }
        public string FolderName { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select a genre")]
        public int Genre { get; set; }


		[ForeignKey("ArtistID")]
		public virtual List<AlbumModel> Albums { get; set;}
        [ForeignKey("Genre")]
        public virtual GenreModel GenreObject { get; set; }
	}
}
