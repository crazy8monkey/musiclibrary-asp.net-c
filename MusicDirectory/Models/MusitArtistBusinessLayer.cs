﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicDirectory.DAL;
using System.Data.Entity;
using System.Diagnostics.Contracts;
using MusicDirectory.Libraries;
using System.Text.RegularExpressions;

namespace MusicDirectory.Models
{
	
	public class MusicArtistBusinessLayer
	{
		private MusicDirectoryDAL musicDal = new MusicDirectoryDAL();
		private FolderDirectory ArtistFolder = new FolderDirectory();

		private string toFriendly(string subject)
		{
			subject = subject.Trim().ToLower();
			subject = Regex.Replace(subject, @"\s+", "-");
			subject = Regex.Replace(subject, @"[^A-Za-z0-9_-]", "");
			return subject;
		}

		public List<ArtistModel> GetMusicArtists()
		{
			return musicDal.MusicArtist.ToList();
		}

		public ArtistModel SaveNewArtist(ArtistModel artist)
		{

			ArtistFolder.CreateArtistDirectory(toFriendly(String.Format("{0}-music", artist.ArtistName)));
			musicDal.MusicArtist.Add(new ArtistModel
			{
				ArtistName = artist.ArtistName,
				FolderName = toFriendly(String.Format("{0}-music", artist.ArtistName)),
                Genre = artist.Genre
			});
			musicDal.SaveChanges();
			return artist;
		}

		public ArtistModel EditArtist(ArtistModel artist) {
			var SingleArtist = musicDal.MusicArtist.FirstOrDefault(a => a.ArtistID == artist.ArtistID);
			SingleArtist.ArtistName = artist.ArtistName;
			musicDal.SaveChanges();
			return artist;
		}

		public ArtistModel DeleteArtist(ArtistModel artist)
		{
			var ArtistToRemove = musicDal.MusicArtist.SingleOrDefault(x => x.ArtistID == artist.ArtistID);
			ArtistFolder.DeleteArtistDirectory(ArtistToRemove.FolderName);
            //delete related albums by artist
            musicDal.MusicArtistAlbum.RemoveRange(musicDal.MusicArtistAlbum.Where(x => x.ArtistID == artist.ArtistID));
			musicDal.MusicArtist.Remove(ArtistToRemove);
			musicDal.SaveChanges();
			return artist;
		}

		public ArtistModel GetSingleArtist(int ID)
		{
			return musicDal.MusicArtist.Find(ID);
		}
	}
}
