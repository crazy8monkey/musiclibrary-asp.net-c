﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MusicDirectory.Models;

namespace MusicDirectory.Models
{
    public class GenreModel
    {
        [Key]
        public int genreID { get; set;  }
        public string GenreText { get; set; }

        public virtual List<ArtistModel> Artists { get; set; }
    }
}
