﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using MusicDirectory.Models;

namespace MusicDirectory.Models
{
    public class AlbumModel
    {
        [Key]
        public int albumID { get; set; }
        public string AlbumName { get; set; }

        public string AlbumFolderName { get; set; }
        public int ArtistID { get; set; }
        public string AlbumArtwork { get; set; }
        public virtual ArtistModel Artist { get; set; }

        [ForeignKey("AlbumID")]
        public virtual List<MusicTrackModel> Tracks {get; set; }
		 
	}
}
