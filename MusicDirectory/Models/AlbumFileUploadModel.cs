﻿using System;
using System.IO;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicDirectory.Models
{
    public class AlbumFileUploadModel
    {
		[Display(Name = "Album Name")]
		[Required(ErrorMessage = "Album Name is Required")]
		public string AlbumName { get; set; }
		public string AlbumFolderName { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select an Artist")]
		public int ArtistID { get; set; }

		public HttpPostedFileBase AlbumArtwork { get; set; }
    }
}
