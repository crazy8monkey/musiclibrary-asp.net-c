﻿using System;
using System.Linq;
using System.IO;
using MusicDirectory.DAL;
using MusicDirectory.Models;
using System.Web;
using System.Collections.Generic;

namespace MusicDirectory.Models
{
    public class MusicTrackBusinessLayer
    {
        private MusicDirectoryDAL musicDal = new MusicDirectoryDAL();

        public MusicTrackUploadModel SaveTrack(MusicTrackUploadModel trackSingle)
        {

            var SingleAlbum = musicDal.MusicArtistAlbum.FirstOrDefault(a => a.albumID == trackSingle.AlbumID);
            var SingleArtist = musicDal.MusicArtist.FirstOrDefault(a => a.ArtistID == SingleAlbum.ArtistID);

            var MusicTrackFile = Path.GetFileName(trackSingle.SongName.FileName);
            var path = Path.Combine(HttpContext.Current.Server.MapPath(String.Format("~/MusicFiles/{0}/{1}", SingleArtist.FolderName, SingleAlbum.AlbumFolderName)), MusicTrackFile);
            trackSingle.SongName.SaveAs(path);

            musicDal.MusicTrack.Add(new MusicTrackModel
            {
                SongName = trackSingle.SongName.FileName,
                AlbumID = trackSingle.AlbumID
            });

            musicDal.SaveChanges();
            return trackSingle;
        }

        public List<MusicTrackModel> GetTracks(int ID)
        {
            return musicDal.MusicTrack.Where(t => t.AlbumID == ID).ToList();
        }

        public MusicTrackModel GetTrack(int trackID) {
            return musicDal.MusicTrack.Find(trackID);
        }

       

    }
}
