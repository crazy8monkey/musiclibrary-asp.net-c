﻿using System;
using System.Web;

namespace MusicDirectory.Models
{
    public class MusicTrackUploadModel
    {
		public HttpPostedFileBase SongName { get; set; }
		public int AlbumID { get; set; }
    }
}
