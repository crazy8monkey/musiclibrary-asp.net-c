﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicDirectory.Models;
using MusicDirectory.ViewModels;

namespace MusicDirectory.Controllers
{
    public class GenreController : Controller
    {
        private GenreBusinessLayer GenreDAL = new GenreBusinessLayer();
        private GenreListViewModel GenreListViewModel = new GenreListViewModel();

        public ActionResult Index()
        {
            ViewData["Title"] = "Genre List";
            List<GenreModel> Genres = GenreDAL.GetGenreLists();
            List<GenreViewModel> GenreViewModels = new List<GenreViewModel>();

            foreach(GenreModel GenreSingle in Genres) {
                GenreViewModel genreSingle = new GenreViewModel();
                genreSingle.genreID = GenreSingle.genreID;
                genreSingle.GenreText = GenreSingle.GenreText;
                genreSingle.Artists = GenreSingle.Artists;

                GenreViewModels.Add(genreSingle);
            }

            GenreListViewModel.GenreList = GenreViewModels;

            return View(GenreListViewModel);
        }

        public ActionResult New() {
            ViewData["Title"] = "Add New Music Genre";
            return View("NewGenre");
        }

        public ActionResult Edit(int ID) {
            ViewData["Title"] = "Edit Music Genre";
            var GenreModel = GenreDAL.SingleGenre(ID);
            return View("EditGenre", GenreModel);
        }

        [HttpPost]
        public ActionResult CreateNewGenre(GenreModel genre) {
            GenreDAL.SaveNewGenre(genre);
            return RedirectToAction("Index", "Genre");
        }

        [HttpPost]
        public ActionResult EditCurrentGenre(GenreModel genre) {
            GenreDAL.EditGenre(genre);
            return RedirectToAction("Index", "Genre");
        }

    }
}
