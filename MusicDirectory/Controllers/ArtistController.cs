﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicDirectory.Models;
using MusicDirectory.DAL;
using MusicDirectory.ViewModels;

namespace MusicDirectory.Controllers
{
    public class ArtistController : Controller
    {

        private MusicArtistBusinessLayer MusicDAL = new MusicArtistBusinessLayer();
        private GenreBusinessLayer GenreObj = new GenreBusinessLayer();
        private CreateArtistViewModel ArtistViewModel = new CreateArtistViewModel();

        public ActionResult Index()
        {
            return View ();
        }

        public ActionResult New() {
            ViewData["Title"] = "Add New Artist";
            List<GenreModel> Genres = GenreObj.GetGenreLists();
            ArtistViewModel.GenreList = Genres;
            return View("NewArtist", ArtistViewModel);
        }

		public ActionResult Edit(int ID)
		{
			var ArtistModel = MusicDAL.GetSingleArtist(ID);
			ViewData["Title"] = "Edit Artist";
			return View("EditArtist", ArtistModel);
		}

		[HttpPost]
		public ActionResult SaveNewArtist(ArtistModel artist)
		{
            ViewData["Title"] = "Add New Artist";
            List<GenreModel> Genres = GenreObj.GetGenreLists();
            ArtistViewModel.GenreList = Genres;
            if (ModelState.IsValid)
			{
				MusicDAL.SaveNewArtist(artist);
				return RedirectToAction("Index", "Home");
            } 
            return View("NewArtist", ArtistViewModel);    


		}

		[HttpPost]
		public ActionResult EditCurrentArtist(ArtistModel artist)
		{
			MusicDAL.EditArtist(artist);
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult Delete(ArtistModel artist)
		{
			MusicDAL.DeleteArtist(artist);
			return RedirectToAction("Index", "Home");
		}

    }
}
