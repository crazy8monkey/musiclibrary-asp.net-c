﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicDirectory.Models;
using MusicDirectory.ViewModels;
using System.Media;

namespace MusicDirectory.Controllers
{
    public class AlbumController : Controller
    {
		private MusicArtistBusinessLayer MusicDAL = new MusicArtistBusinessLayer();
		private AlbumBusinessLayer AlbumDAL = new AlbumBusinessLayer();
        private AlbumViewModel AlbumObject = new AlbumViewModel();
        private MusicTrackBusinessLayer TrackDAL = new MusicTrackBusinessLayer();
        private MusicArtistBusinessLayer MusicArtistDAL = new MusicArtistBusinessLayer();

        public ActionResult Index()
        {
			ViewData["Title"] = "Blah";
            return View();
        }

		public ActionResult NewAlbum()
		{
			List<ArtistModel> artists = MusicDAL.GetMusicArtists();
            AlbumObject.Artists = artists;
			
			ViewData["Title"] = "Add New Album";
			//return View("NewAlbum", artistListViewModel);
            return View("NewAlbum", AlbumObject);
		}

        public ActionResult Edit(int ID) {
			var AlbumModel = AlbumDAL.GetSingleAlbum(ID);
			ViewData["Title"] = "View Album";
            ViewData["Css"] = "/Scripts/css/AlbumController.css";

            AlbumViewModel albumViewModel = new AlbumViewModel();
            albumViewModel.AlbumID = AlbumModel.albumID;
            albumViewModel.ArtistID = AlbumModel.ArtistID;
            albumViewModel.AlbumName = AlbumModel.AlbumName;
            albumViewModel.AlbumFolderName = AlbumModel.AlbumFolderName;
            albumViewModel.AlbumArtwork = AlbumModel.AlbumArtwork;
            albumViewModel.Artist = AlbumModel.Artist;


            List<MusicTrackModel> AlbumTracks = TrackDAL.GetTracks(ID);
            List<MusicTrackViewModel> trackViewModels = new List<MusicTrackViewModel>();

            foreach(MusicTrackModel track in AlbumTracks) {
                MusicTrackViewModel trackSingle = new MusicTrackViewModel();
                trackSingle.musicFileID = track.musicFileID;
                trackSingle.SongName = track.SongName;
                trackSingle.trackOrder = track.trackOrder;
                trackViewModels.Add(trackSingle);
            }

            albumViewModel.Tracks = trackViewModels;

			return View("EditAlbum", albumViewModel);
        }

        public ActionResult Play(int trackID, int artistID, int albumID) {
			ViewData["track"] = trackID;
			ViewData["artist"] = artistID;
            ViewData["album"] = albumID;
            return View("PlaySong"); 
        }

        public ActionResult TrackFile(int trackID, int artistID, int albumID) {
            var muiscTrackObj = TrackDAL.GetTrack(trackID);
            var albumObj = AlbumDAL.GetSingleAlbum(albumID);
            var muiscArtistObj = MusicArtistDAL.GetSingleArtist(artistID);

            var musicFile = Server.MapPath(string.Format("~/MusicFiles/{0}/{1}/{2}", muiscArtistObj.FolderName, albumObj.AlbumFolderName, muiscTrackObj.SongName));
            return File(musicFile, "audio/mp3");
        }

		[HttpPost]
		public ActionResult CreateNewAlbum(AlbumFileUploadModel album)
		{
			List<ArtistModel> artists = MusicDAL.GetMusicArtists();
			AlbumObject.Artists = artists;
            if(ModelState.IsValid) {
				AlbumDAL.CreateNewAlbum(album);
				return RedirectToAction("Index", "Home");    
            }
			return View("NewAlbum", AlbumObject);
		}

        [HttpPost]
        public ActionResult AddSong(MusicTrackUploadModel track) {
            TrackDAL.SaveTrack(track);
            return RedirectToAction("Edit", "Album", new { id = track.AlbumID });
		}

    }
}
