﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using MusicDirectory.Models;
using MusicDirectory.ViewModels;

namespace MusicDirectory.Controllers
{
	public class HomeController : Controller
	{
		private MusicArtistBusinessLayer MusicBAL = new MusicArtistBusinessLayer();

		public ActionResult Index()
		{
			
			MusicArtistListViewModel artistListViewModel = new MusicArtistListViewModel();
			List<ArtistModel> artists = MusicBAL.GetMusicArtists();
			List<MusicArtistViewModel> artistViewModels = new List<MusicArtistViewModel>();

			foreach (ArtistModel artist in artists)
			{
				MusicArtistViewModel artistViewModel = new MusicArtistViewModel();
				artistViewModel.ArtistID = artist.ArtistID;
				artistViewModel.ArtistName = artist.ArtistName;
				artistViewModel.FolderName = artist.FolderName;
				artistViewModel.Albums = artist.Albums;

				artistViewModels.Add(artistViewModel);
			}
			artistListViewModel.MusicArtists = artistViewModels;

			ViewData["Css"] = "/Scripts/css/HomeController.css";
			ViewData["Title"] = "Your Music";
			return View(artistListViewModel);
		}



	}
}
