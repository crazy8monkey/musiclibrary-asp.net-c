﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MusicDirectory
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "NewArist",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "NewArtist", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "NewAlbum",
				url: "{controller}/{action}",
				defaults: new { controller = "Album", action = "NewAlbum" }
			);
		}
	}
}
